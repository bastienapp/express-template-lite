const express = require("express");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const connection = require("./config/db");

app.get("/", (request, response) => {
  response.send("Hello World!");
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server launched on http://localhost:${port}`);
});
